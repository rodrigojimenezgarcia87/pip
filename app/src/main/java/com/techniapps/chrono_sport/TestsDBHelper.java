/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Rodrigo on 15/07/2017.
 */

public class TestsDBHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Tests.db";

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TestsDatabase.TestsEntry.TABLE_NAME + " (" +
                    TestsDatabase.TestsEntry._ID + " INTEGER PRIMARY KEY," +
                    TestsDatabase.TestsEntry.COLUMN_NAME_TITLE + " TEXT," +
                    TestsDatabase.TestsEntry.COLUMN_NAME_TEST_ARRAY + " TEXT)";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TestsDatabase.TestsEntry.TABLE_NAME;

    public TestsDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
