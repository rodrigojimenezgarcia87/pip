/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btnCreateTest;
    Button btnLoadTest;
    TextView tvMainLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sharedPref = getSharedPreferences("TIMER_SETTINGS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.commit();

        tvMainLogo = (TextView) findViewById(R.id.main_logo);

        btnCreateTest = (Button)findViewById(R.id.btn_create_test);
        btnCreateTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchCreateTestActivity = new Intent(MainActivity.this, CreateTestActivity.class);
                startActivity(launchCreateTestActivity);
            }
        });

        btnLoadTest = (Button) findViewById(R.id.btn_load_test);
        btnLoadTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchLoadTestActivity = new Intent(MainActivity.this, LoadTestActivity.class);
                startActivity(launchLoadTestActivity);
            }
        });
    }

}
