/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by Rodrigo on 15/07/2017.
 */

public class LoadTestActivity extends Activity {

    TextView tv_no_test;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_test);

        tv_no_test = (TextView) findViewById(R.id.tv_no_test);

        //generate list
        TestsDBHelper mDbHelper = new TestsDBHelper(getApplicationContext());

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TestsDatabase.TestsEntry.TABLE_NAME, null);

        ArrayList<String[]> listItems = new ArrayList<>();

        if(cursor.moveToFirst()){
            tv_no_test.setVisibility(View.INVISIBLE);

            do{
                String itemTitle = cursor.getString(cursor.getColumnIndexOrThrow(TestsDatabase.TestsEntry.COLUMN_NAME_TITLE));

                String itemId = String.valueOf(cursor.getInt(cursor.getColumnIndexOrThrow(TestsDatabase.TestsEntry._ID)));

                String[] itemInfo = {itemId, itemTitle};

                listItems.add(itemInfo);
            }while(cursor.moveToNext());
        }

        cursor.close();

        //instantiate custom adapter
        TestArrayAdapter adapter = new TestArrayAdapter(listItems, this);

        //handle listview and assign adapter
        ListView listView = (ListView) findViewById(R.id.lv_test);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

}
