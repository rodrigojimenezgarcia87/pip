/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * Created by Rodrigo on 05/07/2017.
 */

public class TimerActivity extends Activity {

    private ProgressBar pbTimer;
    private TextView tvTimer, tv_current_round;
    private Button btn_save_test;
    private EditText tv_test_name;
    private LinearLayout ll_save_test;

    private CountDownTimer countDownTimer;
    private SharedPreferences sharedPref;

    private JSONArray
            numberOfObjectsArray,
            distanceBetweenObjectsArray,
            speedArray,
            exerciseMinutesArray,
            restingTimeArray;

    private int
            numberOfObjects,
            distanceBetweenObjects,
            speed,
            exerciseMinutes,
            restingTime,
            round = 0,
            currentSeconds,
            timeOfExercise,
            numberOfRounds;

    private long currentMS;

    private String clockText;

    private boolean isTimeToRest = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);

        sharedPref = getSharedPreferences("TIMER_SETTINGS", Context.MODE_PRIVATE);
        numberOfRounds = sharedPref.getInt("NUMBER_OF_ROUNDS", 0);

        pbTimer = (ProgressBar) findViewById(R.id.pb_timer);
        tvTimer = (TextView) findViewById(R.id.tv_timer);
        tv_current_round = (TextView) findViewById(R.id.tv_current_round);
        btn_save_test = (Button) findViewById(R.id.btn_save_test);
        tv_test_name = (EditText) findViewById(R.id.tv_test_name);
        ll_save_test = (LinearLayout) findViewById(R.id.ll_save_test);

        tv_current_round.setText("Ronda 1");

        // if is a loaded test hide "save test"
        boolean isTestToLoad = sharedPref.getBoolean("IS_TEST_TO_LOAD", false);
        if(isTestToLoad){
            ll_save_test.setVisibility(View.INVISIBLE);
            btn_save_test.setVisibility(View.INVISIBLE);
        }

        btn_save_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String testName = tv_test_name.getText().toString();


                //btn_save_test.setVisibility(View.GONE);
                // TODO check empty testname
                if (!testName.isEmpty()){
                    ll_save_test.setVisibility(View.INVISIBLE);
                    btn_save_test.setText("TEST GUARDADO");
                    btn_save_test.setAlpha(0.85f);
                    btn_save_test.setEnabled(false);

                    TestsDBHelper mDbHelper = new TestsDBHelper(getApplicationContext());
                    // Gets the data repository in write mode
                    SQLiteDatabase db = mDbHelper.getWritableDatabase();

                    JSONArray saveTestArray = new JSONArray();
                    saveTestArray.put(sharedPref.getInt(("NUMBER_OF_ROUNDS"), 0));
                    saveTestArray.put(sharedPref.getString(("NUMBER_OF_OBJECTS"), "[]"));
                    saveTestArray.put(sharedPref.getString(("DISTANCE_BETWEEN_OBJECTS"), "[]"));
                    saveTestArray.put(sharedPref.getString(("SPEED"), "[]"));
                    saveTestArray.put(sharedPref.getString(("MINUTES"), "[]"));
                    saveTestArray.put(sharedPref.getString(("RESTING_TIME"), "[]"));

                    // Create a new map of values, where column names are the keys
                    ContentValues values = new ContentValues();
                    values.put(TestsDatabase.TestsEntry.COLUMN_NAME_TITLE, testName);
                    values.put(TestsDatabase.TestsEntry.COLUMN_NAME_TEST_ARRAY, saveTestArray.toString());

                    // Insert the new row, returning the primary key value of the new row
                    long newRowId = db.insert(TestsDatabase.TestsEntry.TABLE_NAME, null, values);
                }

            }
        });
        runRoundsLoop();
    }

    public void runRoundsLoop() {

        // Get exercise parameters from sharedPreferences
        try {
            numberOfObjectsArray = new JSONArray(sharedPref.getString("NUMBER_OF_OBJECTS", "[]"));
            distanceBetweenObjectsArray = new JSONArray(sharedPref.getString("DISTANCE_BETWEEN_OBJECTS", "[]"));
            speedArray = new JSONArray(sharedPref.getString("SPEED", "[]"));
            exerciseMinutesArray = new JSONArray(sharedPref.getString("MINUTES", "[]"));
            restingTimeArray = new JSONArray(sharedPref.getString("RESTING_TIME", "[]"));

            if (!isTimeToRest) {
                exerciseMinutes = exerciseMinutesArray.getInt(round);
            } else {
                exerciseMinutes = restingTimeArray.getInt(round - 1);
            }

            distanceBetweenObjects = distanceBetweenObjectsArray.getInt(round);
            speed = speedArray.getInt(round);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        int exerciseSeconds = exerciseMinutes * 60;

        clockText = String.format("%02d:%02d:%02d", exerciseSeconds / 3600,
                (exerciseSeconds % 3600) / 60, (exerciseSeconds % 60));

        pbTimer.setMax(exerciseSeconds);
        tvTimer.setText(clockText);

        timeOfExercise = (exerciseSeconds) * 1000;

        countDownTimer = new CountDownTimer(timeOfExercise, 100) {
            public void onTick(long milliSeconds) {

                currentMS = milliSeconds;
                currentSeconds = (int) (currentMS / 1000);

                if (!isTimeToRest) {
                    checkForBeep(currentMS);
                }

                clockText = String.format("%02d:%02d:%02d", currentSeconds / 3600,
                        (currentSeconds % 3600) / 60, (currentSeconds % 60));

                if (currentSeconds == 10) {
                    // TODO change background color

                }

                tvTimer.setText(clockText);
                pbTimer.setProgress(currentSeconds);
            }

            public void onFinish() {
                tvTimer.setText("00:00:00");
                pbTimer.setProgress(0);
                if (round < numberOfRounds) {
                    round++;
                    tv_current_round.setText("Ronda " + round);
                    countDownTimer.cancel();
                    if (!isTimeToRest) {
                        isTimeToRest = true;
                        tv_current_round.setText("Descanso ronda " + round);
                    } else {
                        isTimeToRest = false;
                    }
                    runRoundsLoop();
                } else {
                    if (!isTimeToRest) {
                        isTimeToRest = true;
                        tv_current_round.setText("Descanso ronda " + round);
                        runRoundsLoop();
                    } else {
                        tvTimer.setText("FIN");
                        countDownTimer.cancel();
                    }

                }
            }
        }.start();
    }

    boolean beeped = false;
    double lastElapsedMS;
    ToneGenerator toneGen1 = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);

    public void checkForBeep(long ms) {

        double limitToBeep = Math.round(distanceBetweenObjects / (speed / 3.6)); // divide by 3.6 to get km/H to m/s

        if (currentSeconds % limitToBeep == 0 && !beeped) {
            toneGen1.startTone(ToneGenerator.TONE_CDMA_PIP, 150);
            // TODO production beep sound (louder)
            beeped = true;
        } else if (currentSeconds != lastElapsedMS) {
            beeped = false;
        }

        lastElapsedMS = currentSeconds;

    }


    @Override
    protected void onStop() {
        super.onStop();
        countDownTimer.onFinish();
    }
}
