/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.provider.BaseColumns;

/**
 * Created by Rodrigo on 15/07/2017.
 */

public class TestsDatabase {

    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private TestsDatabase() {}

    /* Inner class that defines the table contents */
    public static class TestsEntry implements BaseColumns {

        public static final String TABLE_NAME = "tests";
        public static final String COLUMN_NAME_TITLE = "test_name";
        public static final String COLUMN_NAME_TEST_ARRAY = "test_array";

    }

}
