/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Rodrigo on 04/07/2017.
 */

public class CreateTestActivity extends Activity {

    private Button btnEditRounds;
    private TextView tvTituloNumeroRondas;
    private TextView tvApptitle;
    private NumberPicker npNumberOfRounds;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_test);

        btnEditRounds = (Button) findViewById(R.id.btn_edit_rounds);

        npNumberOfRounds = (NumberPicker) findViewById(R.id.npNumberOfRounds);
        npNumberOfRounds.setMinValue(1);
        npNumberOfRounds.setMaxValue(100);

        btnEditRounds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences sharedPref = getSharedPreferences("TIMER_SETTINGS", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("NUMBER_OF_ROUNDS", Integer.parseInt(String.valueOf(npNumberOfRounds.getValue())));
                editor.putInt("CURRENT_ROUND", 1);
                editor.commit();

                Intent launchEditRoundsActivity = new Intent(CreateTestActivity.this, EditRoundsActivity.class);
                startActivity(launchEditRoundsActivity);
            }
        });

    }
}
