package com.techniapps.chrono_sport;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

public class TestArrayAdapter extends BaseAdapter implements ListAdapter {

    private ArrayList<String[]> list = new ArrayList<>();
    private Context context;
    private SharedPreferences sharedPref;


    public TestArrayAdapter(ArrayList<String[]> list, Context context) {
        this.list = list;
        this.context = context;
    }

    SharedPreferences.Editor editor;

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        JSONArray numberOfObjectsArray;
        JSONArray distanceBetweenObjectsArray;
        JSONArray speedArray;
        JSONArray exerciseMinutesArray;
        JSONArray restingTimeArray;

        View view = convertView;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_test, null);

        //Handle TextView and display string from your list
        TextView listItemText = (TextView) view.findViewById(R.id.tv_list_item);
        Button btn_delete = (Button) view.findViewById(R.id.btn_delete_test);
        Button btn_load_test = (Button) view.findViewById(R.id.btn_load_test);

        String[] value = list.get(position);
        // set row title
        listItemText.setText(value[1]);
        // set the ID of the test on the tag buttons attribute
        btn_load_test.setTag(value[0]);
        btn_delete.setTag(value[0]);

        TestsDBHelper mDbHelper = new TestsDBHelper(context);
        final SQLiteDatabase db = mDbHelper.getReadableDatabase();

        sharedPref = context.getSharedPreferences("TIMER_SETTINGS", Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.delete(TestsDatabase.TestsEntry.TABLE_NAME, TestsDatabase.TestsEntry._ID + "=" + v.getTag(), null);

                list.remove(position);
                notifyDataSetChanged();
            }
        });

        btn_load_test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor cursor = db.rawQuery("SELECT * FROM " + TestsDatabase.TestsEntry.TABLE_NAME + " WHERE " + TestsDatabase.TestsEntry._ID + " = " + v.getTag(), null);
                String testToLoad = "";

                cursor.moveToFirst();
                testToLoad = String.valueOf(cursor.getString(cursor.getColumnIndexOrThrow(TestsDatabase.TestsEntry.COLUMN_NAME_TEST_ARRAY)));
                cursor.close();

                try {
                    JSONArray jsonArray = new JSONArray(testToLoad);

                    editor.putInt("NUMBER_OF_ROUNDS", (Integer) jsonArray.get(0));
                    editor.putString("NUMBER_OF_OBJECTS", String.valueOf(jsonArray.get(1)));
                    editor.putString("DISTANCE_BETWEEN_OBJECTS", String.valueOf(jsonArray.get(2)));
                    editor.putString("SPEED", String.valueOf(jsonArray.get(3)));
                    editor.putString("MINUTES", String.valueOf(jsonArray.get(4)));
                    editor.putString("RESTING_TIME", String.valueOf(jsonArray.get(5)));
                    editor.putBoolean("IS_TEST_TO_LOAD", true);
                    editor.commit();

                    Intent launchNextRoundActivity = new Intent(context, TimerActivity.class);
                    context.startActivity(launchNextRoundActivity);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        return view;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
