/*
 * Copyright (c) Techniapps
 */

package com.techniapps.chrono_sport;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;


/**
 * Created by Rodrigo on 05/07/2017.
 */

public class EditRoundsActivity extends Activity {

    private int currentRound;
    private SharedPreferences sharedPref;
    private Button btnNextRound;
    private TextView tv_current_round;
    private TextView tv_title_num_objects, tv_title_distance, tv_title_speed, tv_title_minutes, tv_title_rest;
    private EditText tv_num_objects, tv_distance_between_objects, tv_speed, tv_minutes, tv_rest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_rounds);

        tv_title_num_objects = (TextView) findViewById(R.id.tv_title_num_objects);
        tv_title_distance = (TextView) findViewById(R.id.tv_title_distance);
        tv_title_speed = (TextView) findViewById(R.id.tv_title_speed);
        tv_title_minutes = (TextView) findViewById(R.id.tv_title_minutes);
        tv_title_rest = (TextView) findViewById(R.id.tv_title_rest);

        tv_num_objects = (EditText) findViewById(R.id.tv_num_objects);
        tv_distance_between_objects = (EditText) findViewById(R.id.tv_distance_between_objects);
        tv_speed = (EditText) findViewById(R.id.tv_speed);
        tv_minutes = (EditText) findViewById(R.id.tv_minutes);
        tv_rest = (EditText) findViewById(R.id.tv_rest);
        tv_current_round = (TextView) findViewById(R.id.tv_current_round);

        sharedPref = getSharedPreferences("TIMER_SETTINGS", Context.MODE_PRIVATE);
        currentRound = sharedPref.getInt("CURRENT_ROUND", 1);
        tv_current_round.setText(tv_current_round.getText().toString() + " " + currentRound);
        btnNextRound = (Button) findViewById(R.id.btn_next_round);

        // check ultima ronda a editar
        if (sharedPref.getInt("NUMBER_OF_ROUNDS", 1) == 1 || currentRound == sharedPref.getInt("NUMBER_OF_ROUNDS", 1)) {
            btnNextRound.setText("EMPEZAR EJERCICIO");
        }

        btnNextRound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (userInputIsValid()) {
                    doNextRound();
                }

            }
        });
    }

    private boolean userInputIsValid() {

        int countUserInputErros = 0;

        if (TextUtils.isEmpty(tv_num_objects.getText())) {
            tv_title_num_objects.setTextColor(getResources().getColor(R.color.customColorAccentLight));
            countUserInputErros++;
        } else {
            tv_title_num_objects.setTextColor(getResources().getColor(R.color.customColorWhite));
        }

        if (TextUtils.isEmpty(tv_distance_between_objects.getText())) {
            tv_title_distance.setTextColor(getResources().getColor(R.color.customColorAccentLight));
            countUserInputErros++;
        } else {
            tv_title_distance.setTextColor(getResources().getColor(R.color.customColorWhite));
        }

        if (TextUtils.isEmpty(tv_speed.getText())) {
            tv_title_speed.setTextColor(getResources().getColor(R.color.customColorAccentLight));
            countUserInputErros++;
        } else {
            tv_title_speed.setTextColor(getResources().getColor(R.color.customColorWhite));
        }

        if (TextUtils.isEmpty(tv_minutes.getText())) {
            tv_title_minutes.setTextColor(getResources().getColor(R.color.customColorAccentLight));
            countUserInputErros++;
        } else {
            tv_title_minutes.setTextColor(getResources().getColor(R.color.customColorWhite));
        }

        if (TextUtils.isEmpty(tv_rest.getText())) {
            tv_title_rest.setTextColor(getResources().getColor(R.color.customColorAccentLight));
            countUserInputErros++;
        } else {
            tv_title_rest.setTextColor(getResources().getColor(R.color.customColorWhite));
        }

        if (countUserInputErros > 0) {
            return false;
        } else {
            return true;
        }

    }

    private void doNextRound() {

        sharedPref = getSharedPreferences("TIMER_SETTINGS", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        JSONArray numberOfObjectsArray = null;
        try {
            numberOfObjectsArray = new JSONArray(sharedPref.getString("NUMBER_OF_OBJECTS", "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        numberOfObjectsArray.put(String.valueOf(tv_num_objects.getText()));
        editor.putString("NUMBER_OF_OBJECTS", numberOfObjectsArray.toString());

        JSONArray distanceBetweenObjectsArray = null;
        try {
            distanceBetweenObjectsArray = new JSONArray(sharedPref.getString("DISTANCE_BETWEEN_OBJECTS", "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        distanceBetweenObjectsArray.put(tv_distance_between_objects.getText());
        editor.putString("DISTANCE_BETWEEN_OBJECTS", distanceBetweenObjectsArray.toString());

        JSONArray speedArray = null;
        try {
            speedArray = new JSONArray(sharedPref.getString("SPEED", "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        speedArray.put(tv_speed.getText());
        editor.putString("SPEED", speedArray.toString());

        JSONArray minutesArray = null;
        try {
            minutesArray = new JSONArray(sharedPref.getString("MINUTES", "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        minutesArray.put(tv_minutes.getText());
        editor.putString("MINUTES", minutesArray.toString());

        JSONArray restingTimeArray = null;
        try {
            restingTimeArray = new JSONArray(sharedPref.getString("RESTING_TIME", "[]"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        restingTimeArray.put(tv_rest.getText());
        editor.putString("RESTING_TIME", restingTimeArray.toString());

        if (currentRound == sharedPref.getInt("NUMBER_OF_ROUNDS", 1)) {
            currentRound = sharedPref.getInt("NUMBER_OF_ROUNDS", 1);
            editor.putInt("CURRENT_ROUND", currentRound);
            editor.commit();
            Intent launchNextRoundActivity = new Intent(EditRoundsActivity.this, TimerActivity.class);
            startActivity(launchNextRoundActivity);
        } else {
            currentRound++;
            editor.putInt("CURRENT_ROUND", currentRound);
            editor.commit();
            Intent launchEditRoundsActivity = new Intent(EditRoundsActivity.this, EditRoundsActivity.class);
            startActivity(launchEditRoundsActivity);
        }
    }
}
